<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

use App\Mail\Consultation;


class MailingController extends Controller {


    public function consultation(Request $request) {
        // sleep(5);
        $this->validate($request, [
            'name' => 'required',
            // 'email' => 'array',
            'phone' => 'required',
            'message' => 'required',
        ]);

        Mail::to(env('MAIL_TO_ADDRESS'))->send(new Consultation($request));


        return [ 'status' => 'success', 'message' => 'Сообщение успешно отправлено. Я свяжусь с Вами в ближайшее время.' ];
    }

}
