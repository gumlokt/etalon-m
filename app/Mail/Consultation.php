<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Http\Request;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class Consultation extends Mailable {
    use Queueable, SerializesModels;

    public $form; // any public property defined on Mailable class will automatically be made available to the view

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Request $request) {
        //
        $this->form = $request;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build() {
        return $this->view('emails.consultation');
    }
}
