$('document').ready(function() {


    $('[data-fancybox="testimonials"]').fancybox({
        buttons: [
            "slideShow",
            "arrowLeft",
            "arrowRight",
            "fullScreen",
            "zoom",
            "thumbs",
            //"share",
            // "download",
            "close",
        ],
    });


    // var parallax = function() {
	// 	$(window).stellar();
	// };


    var goToTop = function() {
		$('.js-gotop').on('click', function(event){
			event.preventDefault();
			$('html, body').animate({
				scrollTop: $('html').offset().top
			}, 500);
			return false;
		});
    };

    // parallax();
    goToTop();

    var data = { 'name': $('[name="name"]'), 'email': $('[name="email"]'), 'phone': $('[name="phone"]'), 'message': $('[name="message"]') }

    // $.post( "/consultation", function( data ) {
    //     $( ".result" ).html( data );
    // });

    $('#sendBtn').click(function() {
        $('#sendBtn').attr('disabled', true);
        $("#statusMessage").removeClass('text-danger').html('<i class="fas fa-spinner fa-pulse fa-lg"></i>');

        $('[name="name"]').attr('disabled', true);
        $('[name="email"]').attr('disabled', true);
        $('[name="phone"]').attr('disabled', true);
        $('[name="message"]').attr('disabled', true);

        var url = $('[name="url"]').val(); // get url of form
        var name = $('[name="name"]').val();
        var email = $('[name="email"]').val();
        var phone = $('[name="phone"]').val();
        var message = $('[name="message"]').val();


        if ('' != name && '' != phone && '' != message) {
            $.post({
                url: url,
                data: { name: name, email: email, phone: phone, message: message },
                success: function (data) {
                    if ('success' == data.status) {
                        $('[name="name"]').val('');
                        $('[name="email"]').val('');
                        $('[name="phone"]').val('');
                        $('[name="message"]').val('');
                    }

                    $("#statusMessage").removeClass('text-danger').addClass('text-success').html('<i class="far fa-thumbs-up"></i> ' + data.message);

                    $('[name="name"]').prop("disabled", false);
                    $('[name="email"]').prop("disabled", false);
                    $('[name="phone"]').prop("disabled", false);
                    $('[name="message"]').prop("disabled", false);
                    $('#sendBtn').prop("disabled", false);

                    console.log(data);
                },
                dataType: "json"
            });
            
        } else {
            $("#statusMessage").removeClass('text-success').addClass('text-danger').html('<i class="far fa-hand-point-up"></i> ' + 'Заполните поля отмеченные звёздочкой');

            $('[name="name"]').prop("disabled", false);
            $('[name="email"]').prop("disabled", false);
            $('[name="phone"]').prop("disabled", false);
            $('[name="message"]').prop("disabled", false);
            $('#sendBtn').prop("disabled", false);
        }
    });



});
