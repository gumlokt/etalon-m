    <footer class="footer" id="footer">
        <div class="container">
            <div class="row">
                <div class="col">
                    <span class="text-light">&copy; {{ '2020' == date('Y') ? 2020 : '2020-' . date('Y') }} <span class="text-danger">ООО "Эталон-М" </span></span>
                </div>
                <div class="col text-right">
                    <span class="text-light">Транспортные услуги по России</span>
                </div>
            </div>
        </div>
    </footer>
