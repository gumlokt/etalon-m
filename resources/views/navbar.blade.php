        <!-- Fixed navbar -->
        <nav class="navbar navbar-expand-sm navbar-light bg-light shadow">
            <div class="container">



                <a class="navbar-brand" href="{{ url('/') }}">
                    <!-- <img src="{{ asset('images/logo.png') }}" height="30" class="d-inline-block align-top" alt=""> -->
                    <span class="text-light">
                        <i class="fas fa-home fa-fw text-danger"></i> Главная
                    </span>
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar" aria-controls="navbar" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>


                <div class="collapse navbar-collapse" id="navbar">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item">
                            <!-- <a class="nav-link text-light" href="{{ url('login') }}">
                                <i class="far fa-file-alt fa-fw text-danger"></i> Устав
                            </a> -->
                        </li>
                    </ul>


                    <ul class="navbar-nav justify-content-end">
                        <!-- <li class="nav-item">
                            <a class="nav-link text-light" href="{{ url('/about') }}">
                                <i class="fas fa-info fa-fw text-danger"></i> О копании
                            </a>
                        </li>

                        <li class="nav-item dropdown">
                            <a class="nav-link text-light" href="{{ url('/services') }}">
                                <i class="fas fa-list-ul fa-fw text-danger"></i> Услуги
                            </a>
                        </li> -->

                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle text-light" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fas fa-list-ul fa-fw text-danger"></i> Услуги
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{ url('/services/transport') }}">
                                    <i class="fas fa-car-side fa-fw"></i> Транспортные
                                </a>
                                <a class="dropdown-item" href="{{ url('/services/diesel') }}">
                                    <i class="fas fa-charging-station fa-fw"></i> Дизельгенераторы
                                </a>
                            </div>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link text-light" href="{{ url('/clients') }}">
                                <i class="fas fa-users fa-fw text-danger"></i> Клиенты
                            </a>
                        </li>

                        <!-- <li class="nav-item">
                            <a class="nav-link text-light" href="{{ url('/reviews') }}">
                                <i class="fas fa-comments fa-fw text-danger"></i> Отзывы
                            </a>
                        </li> -->

                        <li class="nav-item">
                            <a class="nav-link text-light" href="{{ url('/licenses') }}">
                                <i class="fas fa-certificate fa-fw text-danger"></i> Лицензии
                            </a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link text-light" href="{{ url('/vacancies') }}">
                                <i class="fas fa-address-book fa-fw text-danger"></i> Вакансии
                            </a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link text-light" href="{{ url('/contacts') }}">
                                <i class="far fa-envelope fa-fw text-danger"></i> Контакты
                            </a>
                        </li>
                    </ul>
                </div>


            </div>
        </nav>
