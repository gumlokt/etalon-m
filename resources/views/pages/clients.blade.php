@extends('layouts.app')

@section('content')
	<section class="clients animated">
		<div class="container">
			<div class="row">
				<div class="col section-heading text-center">
					<h2 class="to-animate fadeInUp animated">
						<i class="fas fa-users fa-sm"></i> Наши клиенты
					</h2>
				</div>
			</div>

			<div class="row">
				<div class="col section-heading text-center to-animate fadeInRight animated">
					<div class="clients__content">
						<div class="clients__item">
							<div class="clients__logo-box">
								<img src="/images/rosneft2.png" alt="РН-Пурнефтегаз" class="clients__logo">
							</div>

                            <h3 class="clients__title">
                                <a href="https://purneftegaz.rosneft.ru/about/Glance/OperationalStructure/Dobicha_i_razrabotka/Zapadnaja_Sibir/purneftegaz/history/" class="clients__link" target="_blank">ООО «РН-Пурнефтегаз» <i class="fas fa-external-link-alt fa-xs"></i></a>
                            </h3>
                        </div>

						<div class="clients__item">
							<div class="clients__logo-box">
								<img src="/images/yamalnefteproduct.png" alt="РН-Пурнефтегаз" class="clients__logo">
							</div>

                            <h3 class="clients__title">
                                <a href="http://www.rosneft-opt.ru/Krupnim_kontragentam/PAO_NK_ROSNEFT_JAMALNEFTEPRODUKT_JAmalo" class="clients__link" target="_blank">ПАО «НК «Роснефть» - Ямалнефтепродукт» <i class="fas fa-external-link-alt fa-xs"></i></a>
                            </h3>
                        </div>

						<div class="clients__item">
							<div class="clients__logo-box">
								<img src="/images/zpec.jpg" alt="РН-Пурнефтегаз" class="clients__logo">
							</div>

                            <h3 class="clients__title">
                                <a href="http://zpec.ru/" class="clients__link" target="_blank">ОАО «Нефтегазовая Корпорация «Чжунмань» <i class="fas fa-external-link-alt fa-xs"></i></a>
                            </h3>
                        </div>

						<div class="clients__item">
							<div class="clients__logo-box">
								<img src="/images/chop.jpg" alt="РН-Пурнефтегаз" class="clients__logo">
							</div>

                            <h3 class="clients__title">
                                <a href="https://mezvahta.ru/vacancy/ooo-chop-rn-oxrana-yamal/" class="clients__link" target="_blank">ООО ЧОП «РН-ОХРАНА-ЯМАЛ» <i class="fas fa-external-link-alt fa-xs"></i></a>
                            </h3>
                        </div>
					</div>


					<button class="btn btn-outline-info to-animate fadeInUp animated btn__back" onclick="window.history.back()"><i class="fas fa-angle-double-left"></i> Назад</button>
				</div>
			</div>
		</div>
    </section>
@endsection
