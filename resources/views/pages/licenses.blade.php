@extends('layouts.app')

@section('content')
	<section class="licenses animated">
		<div class="container">
			<div class="row">
				<div class="col section-heading text-center">
					<h2 class="to-animate fadeInUp animated">
						<i class="fas fa-certificate fa-sm"></i> Наши лицензии
					</h2>
				</div>
			</div>

			<div class="row">
				<div class="col to-animate fadeInRight animated">

					<div class="grid" data-masonry='{ "itemSelector": ".grid-item", "columnWidth": 200 }'>
						<div class="grid-item">
							<a data-fancybox="certificates" href="/images/licenses/slide01.jpg">
								<img class="img-thumbnail shadow" src="/images/licenses/slide01.jpg">
							</a>
						</div>

						<div class="grid-item">
							<a data-fancybox="certificates" href="/images/licenses/slide02.jpg">
								<img class="img-thumbnail shadow" src="/images/licenses/slide02.jpg">
							</a>
						</div>
					</div>

				</div>
			</div>

			<div class="row">
				<div class="col section-heading text-center">
					<button class="btn btn-outline-info to-animate fadeInUp animated btn__back" onclick="window.history.back()"><i class="fas fa-angle-double-left"></i> Назад</button>
				</div>
			</div>
		</div>
    </section>
@endsection
