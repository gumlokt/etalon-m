@extends('layouts.app')

@section('content')
	<section class="animated" data-section="usualPage" id="usualPage">
		<div class="container">
			<div class="row">
				<div class="col section-heading text-center">
					<h2 class="to-animate fadeInUp animated">
						<i class="fas fa-comments fa-sm"></i> Отзывы о нас
					</h2>
				</div>
			</div>

			<div class="row">
				<div class="col section-heading text-center">
					<h3 class="to-animate fadeInUp animated">Страница находится на стадии наполнения контентом...</h3>

					<button class="btn btn-outline-info to-animate fadeInUp animated btn__back" onclick="window.history.back()"><i class="fas fa-angle-double-left"></i> Назад</button>
				</div>
			</div>
		</div>
    </section>
@endsection
