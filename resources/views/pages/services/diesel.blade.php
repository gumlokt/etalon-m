@extends('layouts.app')

@section('content')
	<section class="services animated">
		<div class="container">
			<div class="row">
				<div class="col section-heading text-center">
					<h2 class="to-animate fadeInUp animated">
						<i class="fas fa-charging-station fa-fw"></i> Дизельгенераторы
					</h2>
				</div>
			</div>

			<div class="row">
				<div class="col-12 section-heading to-animate fadeInRight animated">
					<p class="services__description">
						Мы предоставляем в аренду дизельные генераторы мощностью от 5 до 10 кВт YANMAR <small>(с возможностью обслуживания - заправка, замена расходных материалов)</small>.
					</p>
					<p class="services__description">
						С их помощью Вы можете организовать электроснабжение для проведения строительных и ремонтных работ на удалённых объектах (и любых других мероприятий).
					</p>
				</div>

				<div class="col-12 section-heading to-animate fadeInRight animated">
					<img src="/images/dieselgenerator.png" class="rounded image-responsive to-animate fadeInUp animated" alt="dieselgenerator">
				</div>
			</div>

			<div class="row">
				<div class="col section-heading text-center">
					<button class="btn btn-outline-info to-animate fadeInUp animated btn__back" onclick="window.history.back()"><i class="fas fa-angle-double-left"></i> Назад</button>
				</div>
			</div>
		</div>
    </section>
@endsection
