@extends('layouts.app')

@section('content')
	<section class="services animated">
		<div class="container">
			<div class="row">
				<div class="col section-heading text-center">
					<h2 class="to-animate fadeInUp animated">
						<i class="fas fa-car-side fa-fw"></i> Транспортные услуги
					</h2>
				</div>
			</div>

			<div class="row">
				<div class="col section-heading to-animate fadeInRight animated">
					<p class="services__heading">ООО «Эталон-М» предлагает услуги по доставке сотрудников на месторождения</p>

					<p class="services__description">
						Если Вам необходимо срочно командировать и доставить персонал, мы придем на помощь. Мы понимаем, что оперативность перемещения персонала значительно влияет на рабочий процесс.
					</p>

					<p class="services__heading">Как осуществляется перевозка</p>

					<p class="services__description">
						Для перевозки руководителей, офисного или линейного персонала предусмотрены легковые автомобили, автобусы и снегоболотоходы "Хищник", "Север", "ГАЗ 34039", которые привезут Вас или Ваших сотрудников в любой указанный пункт назначения. Так же мы осуществляем автобусные перевозки.
					</p>

					<img src="/images/cars/duster_1.png" class="img-fluid to-animate fadeInUp animated" alt="car image" style="width: 200px;">
					<img src="/images/cars/hunter_1.png" class="img-fluid to-animate fadeInUp animated" alt="car image" style="width: 200px;">
					<img src="/images/cars/niva_1.png" class="img-fluid to-animate fadeInUp animated" alt="car image" style="width: 200px;">
					<img src="/images/cars/picup_1.png" class="img-fluid to-animate fadeInUp animated" alt="car image" style="width: 200px;">
					<img src="/images/cars/sable_1.png" class="img-fluid to-animate fadeInUp animated" alt="car image" style="width: 200px;">

					<p class="services__description">
						Мы подберем оптимальный маршрут, предоставим автомобиль отечественного или иностранного производства по желанию заказчика, быстро и безопасно доставим пассажиров в указанный пункт.
					</p>

					<p class="services__heading">У нас есть <a href="/licenses">лицензии</a> и документы для перевозки пассажиров.</p>

					<button class="btn btn-outline-info to-animate fadeInUp animated btn__back" onclick="window.history.back()"><i class="fas fa-angle-double-left"></i> Назад</button>
				</div>
			</div>
		</div>
    </section>
@endsection
