@extends('layouts.app')

@section('content')
	<section class="licenses animated">
		<div class="container">
			<div class="row">
				<div class="col section-heading text-center">
					<h2 class="to-animate fadeInUp animated">
                        <i class="fas fa-address-book fa-fw"></i> Вакансии
					</h2>
				</div>
			</div>

			<div class="row">
				<div class="col section-heading">
					<h3 class="to-animate fadeInUp animated">Водители: автобуса/микроавтобуса/водитель категории D</h3>
				</div>
			</div>

			<div class="row">
				<div class="col section-heading to-animate fadeInRight animated">
					<p class="services__heading">ЗП от 75 000 на руки</p>
					<p class="services__heading">Требуемый опыт работы: 1–3 года</p>
                    <p class="services__heading">Полная занятость</p>

                    <p class="services__description">
                        Компания Эталон-М – одна из крупнейших транспортных компаний ЯНАО среди коммерческих перевозчиков! Наша дружная команда в связи с расширением Транспортного отдела, объявляет набор Водителей автобуса/микроавтобуса.
					</p>
                </div>
			</div>


            <div class="row">
				<div class="col to-animate fadeInRight animated">
                    <p class="services__heading">Обязанности:</p>
                    <ul class="vacancies to-animate fadeInRight animated">
                        <li class="vacancies__item">профессиональное вождение туристического/корпоративного автобуса/микроавтобуса</li>
                        <li class="vacancies__item">содержание транспортного средства в чистоте</li>

                        <li class="vacancies__item">устранение возникающих во время работы мелких эксплуатационных неисправностей, не требующих разборки агрегатов/механизмов</li>
                        <li class="vacancies__item">своевременное и грамотное оформление путевой, транспортной, технической документации</li>
                        <li class="vacancies__item">ежедневное прохождение предрейсового и послерейсового медицинского осмотра</li>
                    </ul>
				</div>
			</div>


            <div class="row">
				<div class="col to-animate fadeInRight animated">
                    <p class="services__heading">Требования:</p>
                    <ul class="vacancies to-animate fadeInRight animated">
                        <li class="vacancies__item">водительское удостоверение категории "D", с непрерывным стажем от 1 года</li>
                        <li class="vacancies__item">наличие персональной карты для тахографа (СКЗ / ЕСТР) - ОБЯЗАТЕЛЬНО</li>
                        <li class="vacancies__item">опыт работы водителем автобуса от 3 лет</li>
                        <li class="vacancies__item">знание правил перевозки пассажиров по заказам</li>
                        <li class="vacancies__item">умение пользоваться тахографом</li>
                        <li class="vacancies__item">знание ПДД</li>
                        <li class="vacancies__item">личные качества: ответственность, пунктуальность, сдержанность, исполнительность, дисциплинированность, постоянное поддержание внешнего вида, спокойный стиль вождения</li>
                    </ul>
				</div>
			</div>


            <div class="row">
				<div class="col to-animate fadeInRight animated">
                    <p class="services__heading">Условия:</p>
                    <ul class="vacancies to-animate fadeInRight animated">
                        <li class="vacancies__item">месторасположение транспортных баз: г. Губкинский, г. Новый Уренгой</li>
                        <li class="vacancies__item">достойный уровень заработной платы, своевременные выплаты</li>
                        <li class="vacancies__item">оформление согласно ТК РФ</li>
                        <li class="vacancies__item">стабильная занятость круглый год</li>
                        <li class="vacancies__item">работа на новых автобусах 2020 г. в.</li>
                    </ul>
				</div>
			</div>


			<div class="row">
				<div class="col section-heading text-center">
					<button class="btn btn-outline-info to-animate fadeInUp animated btn__back" onclick="window.history.back()"><i class="fas fa-angle-double-left"></i> Назад</button>
				</div>
			</div>
		</div>
    </section>
@endsection
