	<section class="home animated">
		<div class="container">
			<div class="row text-white">
				<div class="col-lg-6">
					<h1 class="to-animate fadeInDown animated home__title">ТРАНСПОРТНЫЕ УСЛУГИ ПО ЯНАО И РОССИИ</h1>

					<p class="mb-4 to-animate fadeInDown animated home__phone">
						<i class="fas fa-phone fa-fw text-danger"></i> +7 (34936) 2-84-29
					</p>

					<p class="mb-0 text-light text-right to-animate fadeInUp animated home__description">
					Оказываем услуги по доставке сотрудников на месторождения легковым и автобусным транспортом, снегоболотоходами
					</p>
				</div>

				<div class="col-lg-6">
					<img src="/images/cars/duster_1.png" class="rounded float-right to-animate fadeInUp animated" alt="car image" style="max-width: 400px;">
				</div>
			</div>


			<div class="row">
				<div class="col">
					<p class="mb-0 text-light to-animate fadeInUp animated home__subtitle text-center">
						Мы предоставляем следующие виды легкового, грузопассажирского, вездеходного транспорта:
					</p>

					<ul class="vehicles to-animate fadeInRight animated text-light">
						<li class="vehicles__item">Toyota LC 200</li>
						<li class="vehicles__item">Toyota Hilux</li>
                        <li class="vehicles__item">Renault Duster</li>

						<li class="vehicles__item">ГАЗ Соболь 4х4</li>
						<li class="vehicles__item">УАЗ Патриот (Пикап)</li>
						<li class="vehicles__item">Шевроле Нива</li>

                        <li class="vehicles__item">Снегоболотоход ГАЗ 34039</li>
						<li class="vehicles__item">Mercedes Benz Sprinter</li>
						<li class="vehicles__item">Автобус ПАЗ 320414-04</li>
						<li class="vehicles__item">Снегоболотоходы Хищник, Север</li>
					</ul>
				</div>
			</div>
		</div>

		<!-- <div class="slant-home"></div> -->
	</section>
