	<section class="intro animated">
		<div class="container">

<!--
			<div class="row">
				<div class="col section-heading">
					<h2 class="to-animate fadeInUp animated">Наши услуги</h2>
				</div>
			</div>
 -->

			<div id="carouselExampleCaptions" class="carousel slide" data-ride="carousel">
				<!-- <ol class="carousel-indicators">
					<li data-target="#carouselExampleCaptions" data-slide-to="0" class="active"></li>
					<li data-target="#carouselExampleCaptions" data-slide-to="1"></li>
					<li data-target="#carouselExampleCaptions" data-slide-to="2"></li>
					<li data-target="#carouselExampleCaptions" data-slide-to="3"></li>
					<li data-target="#carouselExampleCaptions" data-slide-to="4"></li>
					<li data-target="#carouselExampleCaptions" data-slide-to="5"></li>
					<li data-target="#carouselExampleCaptions" data-slide-to="6"></li>
					<li data-target="#carouselExampleCaptions" data-slide-to="7"></li>
					<li data-target="#carouselExampleCaptions" data-slide-to="8"></li>
				</ol> -->

				<div class="carousel-inner">
					<div class="carousel-item active">
						<img src="/images/cars/slide_duster.png" class="d-block w-100" alt="Renault Duster">
						<div class="carousel-caption d-none d-md-block">
							<h5 class="vehicle__name">Renault Duster</h5>
							<!-- <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p> -->
						</div>
					</div>

					<div class="carousel-item">
						<img src="/images/cars/slide_gaz34039.png" class="d-block w-100" alt="Снегоболотоход ГАЗ 34039">
						<div class="carousel-caption d-none d-md-block">
							<h5 class="vehicle__name">Снегоболотоход ГАЗ 34039</h5>
							<!-- <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p> -->
						</div>
					</div>

					<div class="carousel-item">
						<img src="/images/cars/slide_hilux.png" class="d-block w-100" alt="Toyota Hilux">
						<div class="carousel-caption d-none d-md-block">
							<h5 class="vehicle__name">Toyota Hilux</h5>
							<!-- <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p> -->
						</div>
					</div>

					<div class="carousel-item">
						<img src="/images/cars/slide_hunter.png" class="d-block w-100" alt="УАЗ Хантер">
						<div class="carousel-caption d-none d-md-block">
							<h5 class="vehicle__name">УАЗ Хантер</h5>
							<!-- <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p> -->
						</div>
					</div>

					<div class="carousel-item">
						<img src="/images/cars/slide_lc200.png" class="d-block w-100" alt="Toyoyta Land Cruiser 200">
						<div class="carousel-caption d-none d-md-block">
							<h5 class="vehicle__name">Toyoyta Land Cruiser 200</h5>
							<!-- <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p> -->
						</div>
					</div>

					<div class="carousel-item">
						<img src="/images/cars/slide_niva.png" class="d-block w-100" alt="Shevrolet Niva">
						<div class="carousel-caption d-none d-md-block">
							<h5 class="vehicle__name">Shevrolet Niva</h5>
							<!-- <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p> -->
						</div>
					</div>

					<div class="carousel-item">
						<img src="/images/cars/slide_pickup.png" class="d-block w-100" alt="УАЗ Пикап">
						<div class="carousel-caption d-none d-md-block">
							<h5 class="vehicle__name">УАЗ Пикап</h5>
							<!-- <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p> -->
						</div>
					</div>

					<div class="carousel-item">
						<img src="/images/cars/slide_predator.png" class="d-block w-100" alt="Снегоболотоход Хищник">
						<div class="carousel-caption d-none d-md-block">
							<h5 class="vehicle__name">Снегоболотоход Хищник</h5>
							<!-- <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p> -->
						</div>
					</div>

					<div class="carousel-item">
						<img src="/images/cars/slide_sable.png" class="d-block w-100" alt="ГАЗ Соболь 4х4">
						<div class="carousel-caption d-none d-md-block">
							<h5 class="vehicle__name">ГАЗ Соболь 4х4</h5>
							<!-- <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p> -->
						</div>
					</div>

					<div class="carousel-item">
						<img src="/images/cars/slide_sprinter.png" class="d-block w-100" alt="Mercedes Benz Sprinter">
						<div class="carousel-caption d-none d-md-block">
							<h5 class="vehicle__name">Mercedes Benz Sprinter</h5>
							<!-- <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p> -->
						</div>
					</div>

					<div class="carousel-item">
						<img src="/images/cars/slide_paz320414-04.png" class="d-block w-100" alt="Автобус городской ПАЗ 320414-04">
						<div class="carousel-caption d-none d-md-block">
							<h5 class="vehicle__name">Автобус городской ПАЗ 320414-04</h5>
							<!-- <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p> -->
						</div>
					</div>

				</div>

				<!-- <a class="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-slide="prev">
					<span class="carousel-control-prev-icon" aria-hidden="true"></span>
					<span class="sr-only">Назад</span>
				</a>

				<a class="carousel-control-next" href="#carouselExampleCaptions" role="button" data-slide="next">
					<span class="carousel-control-next-icon" aria-hidden="true"></span>
					<span class="sr-only">Вперёд</span>
				</a> -->
			</div>


		</div>
	</section>
