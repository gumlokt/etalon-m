    <section class="animated" id="etalon-licenses">
		<div class="container">


			<div class="row">
				<div class="col-md-12 section-heading">
					<h2 class="to-animate fadeInUp animated">ЛИЦЕНЗИИ</h2>
					<h3>ВСЕ ВИДЫ НАШИ УСЛУГ ЛИЦЕНЗИРОВАНЫ</h3>
				</div>
			</div>

			<div class="row">
				<div class="col">

					<div class="grid" data-masonry='{ "itemSelector": ".grid-item", "columnWidth": 200 }'>
						<div class="grid-item">
							<a data-fancybox="certificates" href="/images/licenses/slide01.jpg">
								<img class="img-thumbnail shadow" src="/images/licenses/slide01.jpg">
							</a>
						</div>

						<div class="grid-item">
							<a data-fancybox="certificates" href="/images/licenses/slide02.jpg">
								<img class="img-thumbnail shadow" src="/images/licenses/slide02.jpg">
							</a>
						</div>
					</div>

				</div>
			</div>


		</div>
    </section>
