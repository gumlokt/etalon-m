	<section class="animated" data-section="partners" id="partners">
		<div class="container">
			<div class="row">
				<div class="col section-heading text-center">
					<h2 class="to-animate fadeInUp animated">Наши партнеры</h2>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6 col-sm-6 service to-animate fadeInUp animated">
					<i class="icon fas fa-building fa-4x bounceIn animated"></i>
					<h3>Застройщики</h3>
					<p>Здесь будет контент в несколько строк - буквально два-три предложения. Здесь будет контент в несколько строк - буквально два-три предложения. Здесь будет контент в несколько строк - буквально два-три предложения. Здесь будет контент в несколько строк - буквально два-три предложения.</p>
				</div>
				
				<div class="col-md-6 col-sm-6 service to-animate fadeInUp animated">
					<i class="icon fas fa-university fa-4x bounceIn animated"></i>
					<h3>Банки</h3>
					<p>Здесь будет контент в несколько строк - буквально два-три предложения. Здесь будет контент в несколько строк - буквально два-три предложения. Здесь будет контент в несколько строк - буквально два-три предложения. Здесь будет контент в несколько строк - буквально два-три предложения.</p>
				</div>	
				
				<div class="clearfix visible-sm-block"></div>
				
				<div class="col-md-6 col-sm-6 service to-animate fadeInUp animated">
					<i class="icon fas fa-city fa-4x bounceIn animated"></i>
					<h3>Госучреждения</h3>
					<p>Здесь будет контент в несколько строк - буквально два-три предложения. Здесь будет контент в несколько строк - буквально два-три предложения. Здесь будет контент в несколько строк - буквально два-три предложения. Здесь будет контент в несколько строк - буквально два-три предложения.</p>
				</div>

				<div class="col-md-6 col-sm-6 service to-animate fadeInUp animated">
					<i class="icon fas fa-hands-helping fa-4x bounceIn animated"></i>
					<h3>Агентства</h3>
					<p>Здесь будет контент в несколько строк - буквально два-три предложения. Здесь будет контент в несколько строк - буквально два-три предложения. Здесь будет контент в несколько строк - буквально два-три предложения. Здесь будет контент в несколько строк - буквально два-три предложения.</p>
				</div>
			</div>
		</div>
	</section>
