<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\IndexController;
use App\Http\Controllers\MailingController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });


// Route::get('/', 'IndexController@index');
Route::get('/', [IndexController::class, 'index']);


// Nav bar links
Route::get('/about', function () {
    return view('pages.about');
});

Route::get('/services/transport', function () {
    return view('pages.services.transport');
});

Route::get('/services/diesel', function () {
    return view('pages.services.diesel');
});

Route::get('/clients', function () {
    return view('pages.clients');
});

Route::get('/reviews', function () {
    return view('pages.reviews');
});

Route::get('/licenses', function () {
    return view('pages.licenses');
});

Route::get('/vacancies', function () {
    return view('pages.vacancies');
});

Route::get('/contacts', function () {
    return view('pages.contacts');
});




// Intro links
// Route::get('/sale', function () {
//     return view('pages.sale');
// });

// Route::get('/purchase', function () {
//     return view('pages.purchase');
// });

// Route::get('/exchange', function () {
//     return view('pages.exchange');
// });


// Route::get('/paperwork', function () {
//     return view('pages.paperwork');
// });

// Route::get('/safedeal', function () {
//     return view('pages.safedeal');
// });

// Route::get('/mortgage', function () {
//     return view('pages.mortgage');
// });


// Mailing
// Route::post('/mail/consultation', 'App\\Http\\Controllers\\MailingController@consultation');
Route::post('/mail/consultation', [MailingController::class, 'consultation']);
// Route::post('/mail/consultation', 'MailingController@consultation');
// Route::post('/mail/purchase', 'MailingController@purchase');
// Route::post('/mail/sale', 'MailingController@sale');
